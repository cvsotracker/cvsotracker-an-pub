/*
 * Created by Marek Stoma on 3/5/20 2:30 AM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 3/5/20 2:28 AM
 */

package org.b25b_48c5_9914_c4878609efe9.cvsotracker.ui.dashboard

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_total.view.*
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.R
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.model.NCovData
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.model.NCovDataItem
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.viewmodel.NCovViewModel
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class DashboardFragment : Fragment() {

    private lateinit var viewModel: NCovViewModel

    private val loadingValue = "..."
    private val noValue = "-"

    private var searchContentView: RecyclerView? = null
    private val searchContent = ArrayList<NCovDataItem>()

    private val percentFormatter = NumberFormat.getPercentInstance()
    private val numberFormatter = NumberFormat.getNumberInstance()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(requireActivity()).get(NCovViewModel::class.java)
        viewModel.stateSelectedItem.value = null

//        percentFormatter.maximumFractionDigits = 2

        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        val filterButton: ImageButton = root.findViewById(R.id.filterButton)

        filterButton.visibility = View.GONE
        filterButton.setOnClickListener {
            it.isSelected = !it.isSelected
        }

        setupTotalView(
            root,
            root.findViewById(R.id.confirmed),
            NCovViewModel.NCovOption.Confirmed,
            viewModel.outputTotalConfirmed(),
            null
        )

        setupTotalView(
            root,
            root.findViewById(R.id.active),
            NCovViewModel.NCovOption.Active,
            viewModel.outputActiveCases(),
            viewModel.outputTotalConfirmed()
        )

        setupTotalView(
            root,
            root.findViewById(R.id.deaths),
            NCovViewModel.NCovOption.Deaths,
            viewModel.outputTotalDeaths(),
            viewModel.outputTotalConfirmed()
        )

        setupTotalView(
            root,
            root.findViewById(R.id.recovered),
            NCovViewModel.NCovOption.Recovered,
            viewModel.outputTotalRecovered(),
            viewModel.outputTotalConfirmed()
        )

        val searchContentView: RecyclerView = root.findViewById(R.id.searchContent)
        this.searchContentView = searchContentView
        setupSearchContentView(
            requireContext(),
            searchContentView,
            root.findViewById(R.id.search)
        )

        return root
    }

    private fun setupTotalView(root: View,
                               view: View,
                               option: NCovViewModel.NCovOption,
                               liveData: LiveData<NCovData>,
                               dependencyData: LiveData<NCovData>?) {
        view.title.text = resources.getText(option.titleTotalRes())
        view.title.setTextColor(Color.GRAY)
        view.title_info.setTextColor(ContextCompat.getColor(root.context, option.colorRes()))
        view.subtitle.setTextColor(ContextCompat.getColor(root.context, option.colorRes()))
        view.content_icon.setColorFilter(option.colorRes())
        view.content.visibility = View.GONE

        view.title_info.visibility = View.GONE
        view.subtitle.text = loadingValue
        view.subtitle.setOnClickListener {
            viewModel.stateOption.value = option
        }

        viewModel.stateOption.observe(viewLifecycleOwner, {
            view.totalContent?.isSelected = (option == it)
        })

        fun updateSubtitleInfo() {
            if (dependencyData == null) return

            val latest = liveData.value?.latest ?: 0
            val latestBase = dependencyData.value?.latest ?: 0

            if (latestBase > 0) {
                val percents = latest.toDouble() / latestBase.toDouble()
                view.title_info.text = percentFormatter.format(percents)
                view.title_info.visibility = View.VISIBLE
            }
            else {
                view.title_info.visibility = View.GONE
            }
        }

        liveData.observe(viewLifecycleOwner, {
            if (it == null) {
                view.subtitle.text = noValue
            } else {
                view.subtitle.text = numberFormatter.format(it.latest)
                updateSubtitleInfo()
            }
        })
        dependencyData?.observe(viewLifecycleOwner, {
            updateSubtitleInfo()
        })
    }

    private fun setupSearchContentView(context: Context,
                                       view: RecyclerView,
                                       search: EditText) {
        val itemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        val lineDecoration = ContextCompat.getDrawable(context, R.drawable.line_decoration)
        if (lineDecoration != null) {
            itemDecoration.setDrawable(lineDecoration)
        }
        view.addItemDecoration(itemDecoration)

        val adapter = SearchContentRecyclerViewAdapter(
            searchContent,
            context,
            ContextCompat.getColor(context, R.color.colorWhite),
            null
        )
        view.layoutManager = LinearLayoutManager(context)
        view.adapter = adapter

        fun setupSearchContent() {
            val option = viewModel.stateOption.value ?: return

            val liveData = when(option) {
                NCovViewModel.NCovOption.Confirmed -> viewModel.outputTotalConfirmed()
                NCovViewModel.NCovOption.Deaths -> viewModel.outputTotalDeaths()
                NCovViewModel.NCovOption.Recovered -> viewModel.outputTotalRecovered()
                NCovViewModel.NCovOption.Active -> viewModel.outputActiveCases()
            }
            val filter = viewModel.stateSearchContentFilter.value

            searchContent.clear()
            val data = (liveData.value?.locations ?: ArrayList())
            if (filter != null && filter.trim().isNotEmpty()) {
                val filterLowerCase = filter.toLowerCase(Locale.ROOT)
                searchContent.addAll(data.filter {
                    it.province.toLowerCase(Locale.ROOT).contains(filterLowerCase) ||
                    it.country.toLowerCase(Locale.ROOT).contains(filterLowerCase)
                })
            } else {
                searchContent.addAll(data)
            }

            adapter.contentColorRes = ContextCompat.getColor(context, option.colorRes())
            adapter.notifyDataSetChanged()
        }

        search.text = Editable.Factory.getInstance()
            .newEditable(viewModel.stateSearchContentFilter.value ?: "")
        search.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.stateSearchContentFilter.value = p0.toString()
            }
        })

        adapter.selectListener = object: SearchContentRecyclerViewAdapter.OnSelectListener {
            override fun onSelectItem(item: NCovDataItem) {
                viewModel.stateSelectedItem.value = item
                findNavController().navigate(R.id.navigation_map)
            }
        }

        viewModel.stateOption.observe(viewLifecycleOwner, {
            setupSearchContent()
        })
        viewModel.stateSearchContentFilter.observe(viewLifecycleOwner, {
            setupSearchContent()
        })

        viewModel.outputTotalConfirmed().observe(viewLifecycleOwner, {
            if (viewModel.stateOption.value == NCovViewModel.NCovOption.Confirmed) {
                setupSearchContent()
            }
        })
        viewModel.outputActiveCases().observe(viewLifecycleOwner, {
            if (viewModel.stateOption.value == NCovViewModel.NCovOption.Active) {
                setupSearchContent()
            }
        })
        viewModel.outputTotalDeaths().observe(viewLifecycleOwner, {
            if (viewModel.stateOption.value == NCovViewModel.NCovOption.Deaths) {
                setupSearchContent()
            }
        })
        viewModel.outputTotalRecovered().observe(viewLifecycleOwner, {
            if (viewModel.stateOption.value == NCovViewModel.NCovOption.Recovered) {
                setupSearchContent()
            }
        })
    }
}
