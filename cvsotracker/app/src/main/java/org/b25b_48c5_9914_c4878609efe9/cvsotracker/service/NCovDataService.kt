/*
 * Created by Marek Stoma on 3/5/20 2:30 AM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 3/5/20 2:28 AM
 */

package org.b25b_48c5_9914_c4878609efe9.cvsotracker.service

import com.google.gson.*
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.model.NCovData
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.model.NCovDataItemCoordinates
import retrofit2.Retrofit
import retrofit2.Call
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.lang.Exception
import java.lang.reflect.Type
import java.util.concurrent.Executors


interface NCovDataService {

    @GET("data?c=confirmed")
    fun getConfirmed(): Call<NCovData>

    @GET("data?c=deaths")
    fun getDeaths(): Call<NCovData>

    @GET("data?c=recovered")
    fun getRecovered(): Call<NCovData>

    object Factory {
        fun create(): NCovDataService {
            val cvsotrackerBaseUrl = "https://webhooks.mongodb-stitch.com/api/client/v2.0/app/"
            val cvsotrackerServiceUrl = "cvsotracker-vunhm/service/cvso/incoming_webhook/"
            val retrofit = Retrofit.Builder()
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(cvsotrackerBaseUrl + cvsotrackerServiceUrl)
                .build()
            return retrofit.create(NCovDataService::class.java)
        }
    }
}

interface NCovDataServiceExternal {

    @GET("confirmed")
    fun getConfirmed(): Call<NCovData>

    @GET("deaths")
    fun getDeaths(): Call<NCovData>

    @GET("recovered")
    fun getRecovered(): Call<NCovData>

    object Factory {
        fun create(): NCovDataServiceExternal {
            val cvsotrackerBaseUrl = "https://coronavirus-tracker-api.herokuapp.com/"
            val cvsotrackerServiceUrl = ""
            val gson = GsonBuilder()
                .setLenient()
                .registerTypeAdapter(
                    NCovDataItemCoordinates::class.java,
                    RetrofitNCovDataItemCoordinatesDeserializer()
                )
                .create()
            val retrofit = Retrofit.Builder()
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .callbackExecutor(Executors.newSingleThreadExecutor())
                .baseUrl(cvsotrackerBaseUrl + cvsotrackerServiceUrl)
                .build()
            return retrofit.create(NCovDataServiceExternal::class.java)
        }
    }
}

class RetrofitNCovDataItemCoordinatesDeserializer : JsonDeserializer<NCovDataItemCoordinates?> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): NCovDataItemCoordinates {
        var lat: Double? = null
        var long: Double? = null
        try {
            val jsonObject = json?.asJsonObject
            lat = jsonObject?.get("lat")?.asDouble
            long = jsonObject?.get("long")?.asDouble
        } catch (e: Exception) {
        } finally {
            if (lat != null && long != null) {
                return NCovDataItemCoordinates(lat, long)
            }
            return NCovDataItemCoordinates(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY)
        }
    }
}