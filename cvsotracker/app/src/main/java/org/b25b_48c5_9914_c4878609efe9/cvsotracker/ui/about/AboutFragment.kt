/*
 * Created by Marek Stoma on 3/5/20 2:30 AM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 3/5/20 2:28 AM
 */

package org.b25b_48c5_9914_c4878609efe9.cvsotracker.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.R
import mehdi.sakout.aboutpage.AboutPage
import mehdi.sakout.aboutpage.Element
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.BuildConfig

class AboutFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val version = resources.getString(R.string.about_version)
        val build = resources.getString(R.string.about_build)
//        val feedback = resources.getString(R.string.about_feedback)
//        val legal = resources.getString(R.string.about_legal)
        val term = resources.getString(R.string.about_ToU)
//        val policy = resources.getString(R.string.about_PP)
        val openSource = resources.getString(R.string.about_opensource)
//        val dataSource = resources.getString(R.string.about_datasource)
//        val dailyReports = resources.getString(R.string.about_dailyReports)
//        val timeSeries = resources.getString(R.string.about_timeSeries)
//        val repository = resources.getString(R.string.about_repository)
        val other = resources.getString(R.string.about_other)

        val fullVersion  = version + BuildConfig.VERSION_NAME + " " + build + BuildConfig.VERSION_CODE

        return AboutPage(context)
            .isRTL(false)
            .setImage(R.mipmap.ic_launcher_foreground)
            .setDescription(fullVersion + "\n\n" + term)
//            .addGroup(feedback)
//            .addPlayStore(BuildConfig.APPLICATION_ID)
//            .addEmail("cvsotracker@gmail.com")
//            .addGroup(legal)
//            .addWebsite( "https://cvsotracker.github.io/applanding/legal/ToU.html", term)
//            .addWebsite( "https://cvsotracker.github.io/applanding/legal/privacyPolicy.html", policy)
            .addGroup(openSource)
            .addWebsite( "https://square.github.io/retrofit/#license", "Retrofit")
            .addWebsite( "https://github.com/medyo/android-about-page/blob/master/README.md#license", "Android About Page")
//            .addGroup(dataSource)
            .addGroup(other)
            .addWebsite("https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports/", "World Health Organization (WHO)")
            .addItem(Element())
            .create()
    }
}
