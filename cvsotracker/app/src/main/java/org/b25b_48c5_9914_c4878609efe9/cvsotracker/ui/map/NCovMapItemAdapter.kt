/*
 * Created by Marek Stoma on 3/11/20 12:55 AM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 3/11/20 12:55 AM
 */

package org.b25b_48c5_9914_c4878609efe9.cvsotracker.ui.map

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.item_info_map.view.*
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.R


data class NCovMapItem(
    val activeInfo: String,
    val confirmed: String,
    val deaths: String,
    val recovered: String
)

class NCovMapItemAdapter(private val inflater: LayoutInflater): GoogleMap.InfoWindowAdapter {

    override fun getInfoContents(p0: Marker?): View? {
        return null
    }

    override fun getInfoWindow(p0: Marker?): View? {
        val root = inflater.inflate(R.layout.item_info_map, null)

        val title: TextView = root.title
        val subtitle: TextView = root.subtitle
        val confirmed: TextView = root.confirmed
        val deaths: TextView = root.deaths
        val recovered: TextView = root.recovered
        val container: View = root.container

        val mapItem = p0?.tag as NCovMapItem?

        title.text = p0?.title ?: "-"
        subtitle.text = p0?.snippet ?: "-"

        if (mapItem != null) {
            subtitle.text = mapItem.activeInfo

            container.visibility = View.VISIBLE

            confirmed.text = mapItem.confirmed
            deaths.text = mapItem.deaths
            recovered.text = mapItem.recovered
        } else {
            container.visibility = View.GONE
        }

        return root
    }
}