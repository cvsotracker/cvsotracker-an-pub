/*
 * Created by Marek Stoma on 3/5/20 2:30 AM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 3/5/20 2:28 AM
 */

package org.b25b_48c5_9914_c4878609efe9.cvsotracker.ui.map

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.R
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.model.NCovDataItem
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.model.NCovDataValidator
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.viewmodel.NCovViewModel
import java.text.NumberFormat
import kotlin.math.log10
import kotlin.math.max

class NCovMapFragment : Fragment() {

    private lateinit var viewModel: NCovViewModel
    private var bitmapCache = mapOf(
        NCovViewModel.NCovOption.Confirmed to mutableMapOf(),
        NCovViewModel.NCovOption.Deaths to mutableMapOf(),
        NCovViewModel.NCovOption.Recovered to mutableMapOf(),
        NCovViewModel.NCovOption.Active to mutableMapOf<Int, BitmapDescriptor>()
    )
    private var selectedMarker: Marker? = null
    private var animateCamera: Boolean = false

    private val percentFormatter = NumberFormat.getPercentInstance()
    private val numberFormatter = NumberFormat.getNumberInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(requireActivity()).get(NCovViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_map, container, false)

        setupSection(
            root.findViewById(R.id.section)
        )

        setupMap(
            root,
            inflater,
            childFragmentManager.findFragmentById(R.id.googleMap) as? SupportMapFragment
        )

        return root
    }

    private fun setupSection(view: RadioGroup) {
        view.setOnCheckedChangeListener { _, i ->
            when(i) {
                R.id.confirmed -> viewModel.stateOption.value = NCovViewModel.NCovOption.Confirmed
                R.id.deaths -> viewModel.stateOption.value = NCovViewModel.NCovOption.Deaths
                R.id.recovered -> viewModel.stateOption.value = NCovViewModel.NCovOption.Recovered
                R.id.active -> viewModel.stateOption.value = NCovViewModel.NCovOption.Active
            }
        }
        viewModel.stateOption.observe(viewLifecycleOwner, {
            if (it != null) {
                when (it) {
                    NCovViewModel.NCovOption.Confirmed -> view.check(R.id.confirmed)
                    NCovViewModel.NCovOption.Deaths -> view.check(R.id.deaths)
                    NCovViewModel.NCovOption.Recovered -> view.check(R.id.recovered)
                    NCovViewModel.NCovOption.Active -> view.check(R.id.active)
                }
            }
        })
    }

    private fun setupMap(root: View, inflater: LayoutInflater, map: SupportMapFragment?) {
        map?.getMapAsync { googleMap ->
            val isDarkMapStyle = TypedValue()
            val result = root.context.theme.resolveAttribute(R.attr.isDarkMapStyle, isDarkMapStyle, false)
            if (result) {
                val options = MapStyleOptions.loadRawResourceStyle(root.context, R.raw.map_style_night)
                googleMap.setMapStyle(options)
            }
            googleMap.setInfoWindowAdapter(NCovMapItemAdapter(inflater))

            fun getMarkerIconFromDrawable(drawable: Drawable,
                                          color: Int,
                                          absoluteMaxValue: Int?,
                                          value: Int,
                                          dpi: Float,
                                          option: NCovViewModel.NCovOption): BitmapDescriptor {
                var size = (10.0 * dpi).toInt()

                if (absoluteMaxValue != null && absoluteMaxValue > 0) {
                    // base is 0.35% from absoluteMaxValue
                    val log10Base = absoluteMaxValue.toDouble() * 0.0035
                    val multiplier = log10(log10Base * (value.toDouble() / absoluteMaxValue.toDouble()))
                    val maxSize = (40.0 * dpi) * multiplier
                    size = max(size.toDouble(), maxSize).toInt()
                }

                val descriptor = bitmapCache[option]?.get(size)
                if (descriptor != null) {
                    return descriptor
                }

                val canvas = Canvas()
                val bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888)
                canvas.setBitmap(bitmap)
                drawable.setTint(color)
                drawable.setBounds(0, 0, size, size)
                drawable.draw(canvas)

                val newDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap)

                bitmapCache[option]?.put(size, newDescriptor)

                return newDescriptor
            }

            fun snippetForActiveItem(item: NCovDataItem): NCovMapItem {
                val confirmed = viewModel.outputTotalConfirmed().value?.locations?.find {
                    (it.country.compareTo(item.country) == 0 && it.province.compareTo(item.province) == 0)
                }
                val deaths = viewModel.outputTotalDeaths().value?.locations?.find {
                    it.country.compareTo(item.country) == 0 && it.province.compareTo(item.province) == 0
                }
                val recovered = viewModel.outputTotalRecovered().value?.locations?.find {
                    it.country.compareTo(item.country) == 0 && it.province.compareTo(item.province) == 0
                }
                val noValue = "--"
                val latestBase = confirmed?.latest ?: 0
                var confirmedSnippet = resources.getString(NCovViewModel.NCovOption.Confirmed.titleRes()) + ": "
                confirmedSnippet += if (confirmed?.latest != null) numberFormatter.format(confirmed.latest) else noValue
                var deathsSnippet = resources.getString(NCovViewModel.NCovOption.Deaths.titleRes()) + ": "
                if (deaths?.latest != null) {
                    deathsSnippet += numberFormatter.format(deaths.latest)
                    if (latestBase > 0) {
                        deathsSnippet += " (" + percentFormatter.format(deaths.latest.toDouble() / latestBase.toDouble()) + ")"
                    }
                } else {
                    deathsSnippet += noValue
                }
                var recoveredSnippet = resources.getString(NCovViewModel.NCovOption.Recovered.titleRes()) + ": "
                if (recovered?.latest != null) {
                    recoveredSnippet += numberFormatter.format(recovered.latest)
                    if (latestBase > 0) {
                        recoveredSnippet += " (" + percentFormatter.format(recovered.latest.toDouble() / latestBase.toDouble()) + ")"
                    }
                } else {
                    recoveredSnippet += noValue
                }
                var activeSnippet = resources.getString(NCovViewModel.NCovOption.Active.titleRes()) + ": " + numberFormatter.format(item.latest)
                if (latestBase > 0) {
                    activeSnippet += " (" + percentFormatter.format(item.latest.toDouble() / latestBase.toDouble()) + ")"
                }
                return NCovMapItem(activeSnippet, confirmedSnippet, deathsSnippet, recoveredSnippet)
            }

            fun setupMapContent() {
                val option = viewModel.stateOption.value ?: return

                val liveData = when(option) {
                    NCovViewModel.NCovOption.Confirmed -> viewModel.outputTotalConfirmed()
                    NCovViewModel.NCovOption.Deaths -> viewModel.outputTotalDeaths()
                    NCovViewModel.NCovOption.Recovered -> viewModel.outputTotalRecovered()
                    NCovViewModel.NCovOption.Active -> viewModel.outputActiveCases()
                }
                val color = ContextCompat.getColor(root.context, option.colorRes())

                val absoluteMaxValue = viewModel.outputTotalConfirmed().value?.latest
                val dpi = resources.displayMetrics.density

                googleMap.clear()

                val selectedItem = viewModel.stateSelectedItem.value
                liveData.value?.locations?.forEach {
                    val markerDrawable = ResourcesCompat.getDrawable(resources, R.drawable.ic_circle_black_10dp, null)
                    if (markerDrawable == null || NCovDataValidator.isNotValid(it.coordinates)) { return@forEach }

                    var title = it.province

                    if (it.province.isEmpty()) { title = it.country }
                    val markerOptions = MarkerOptions()
                        .position(LatLng(it.coordinates.lat, it.coordinates.long))
                        .title(title)
                        .snippet(resources.getString(option.titleRes()) + ": " + numberFormatter.format(it.latest))
                        .icon(
                            getMarkerIconFromDrawable(
                                markerDrawable,
                                color,
                                absoluteMaxValue,
                                it.latest,
                                dpi,
                                option
                            )
                        )
                        .anchor(0.5f,0.5f)
                        .alpha(0.5f)

                    val marker = googleMap.addMarker(markerOptions)
                    if (option == NCovViewModel.NCovOption.Active) {
                        marker.tag = snippetForActiveItem(it)
                    }
                    if (selectedItem != null && NCovDataValidator.isValid(selectedItem.coordinates)) {
                        val position = LatLng(selectedItem.coordinates.lat, selectedItem.coordinates.long)
                        if (position == markerOptions.position) {
                            selectedMarker = marker
                            if (!animateCamera) {
                                marker.showInfoWindow()
                            }
                        }
                    }
                }
            }

            selectedMarker = null

            viewModel.stateSelectedItem.observe(viewLifecycleOwner, {
                selectedMarker?.hideInfoWindow()
                if (it != null && NCovDataValidator.isValid(it.coordinates)) {
                    val cameraPosition = CameraPosition.builder()
                        .target(LatLng(it.coordinates.lat, it.coordinates.long))
                        .zoom(6.0f)
                        .build()
                    animateCamera = true
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), object: GoogleMap.CancelableCallback{
                        override fun onFinish() {
                            selectedMarker?.showInfoWindow()
                            animateCamera = false
                        }

                        override fun onCancel() {
                            selectedMarker?.showInfoWindow()
                            animateCamera = false
                        }
                    })
                }
            })
            viewModel.stateOption.observe(viewLifecycleOwner, {
                setupMapContent()
            })

            viewModel.outputTotalConfirmed().observe(viewLifecycleOwner, {
                setupMapContent()
            })
            viewModel.outputTotalDeaths().observe(viewLifecycleOwner, {
                val option = viewModel.stateOption.value
                if (option == NCovViewModel.NCovOption.Confirmed || option == NCovViewModel.NCovOption.Deaths) {
                    setupMapContent()
                }
            })
            viewModel.outputTotalRecovered().observe(viewLifecycleOwner, {
                val option = viewModel.stateOption.value
                if (option == NCovViewModel.NCovOption.Confirmed || option == NCovViewModel.NCovOption.Recovered) {
                    setupMapContent()
                }
            })
            viewModel.outputActiveCases().observe(viewLifecycleOwner, {
                if (viewModel.stateOption.value == NCovViewModel.NCovOption.Active) {
                    setupMapContent()
                }
            })
        }
    }
}
