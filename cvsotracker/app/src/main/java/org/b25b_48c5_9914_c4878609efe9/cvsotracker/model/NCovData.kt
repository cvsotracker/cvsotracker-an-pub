/*
 * Created by Marek Stoma on 3/5/20 2:30 AM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 3/5/20 2:28 AM
 */

package org.b25b_48c5_9914_c4878609efe9.cvsotracker.model

import androidx.lifecycle.LiveData
import androidx.room.*

// Main Data

data class NCovDataItemCoordinates (
    val lat: Double,
    val long: Double
)

data class NCovDataItem (
    val country: String,
    val province: String,
    val latest: Int,

    val coordinates: NCovDataItemCoordinates
//    val history: List<...>
)

data class NCovData (
    val latest: Int,
    val locations: List<NCovDataItem>
)

class NCovDataValidator {

    companion object {

        fun isValid(coordinates: NCovDataItemCoordinates): Boolean {
            return (coordinates.lat != Double.POSITIVE_INFINITY && coordinates.long != Double.POSITIVE_INFINITY)
        }

        fun isNotValid(coordinates: NCovDataItemCoordinates): Boolean {
            return !isValid(coordinates)
        }
    }
}

// Room DAO

@Entity
data class NCovDataEntity (
    @PrimaryKey
    var dataId: Long,
    var overallLatest: Int
)

@Entity(
    indices = [
        Index(value = ["lat", "_long"], unique = true)
    ]
)
data class NCovDataItemCoordinatesEntity (
    @PrimaryKey(autoGenerate = true)
    var coordinateId: Long,
    var lat: Double,
    var _long: Double
)

@Entity(
    indices = [
        Index(value = ["country", "province"], unique = true)
    ]
)
data class NCovDataItemPlaceEntity (
    @PrimaryKey(autoGenerate = true)
    var placeId: Long,
    var country: String,
    var province: String
)

@Entity(
    indices = [
        Index(value = ["coordinateRefId", "placeRefId", "dataRefId"], unique = true),
        Index(value = ["coordinateRefId"]),
        Index(value = ["placeRefId"]),
        Index(value = ["dataRefId"])
    ],
    foreignKeys = [
        ForeignKey(
            entity = NCovDataItemCoordinatesEntity::class,
            parentColumns = ["coordinateId"],
            childColumns = ["coordinateRefId"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = NCovDataItemPlaceEntity::class,
            parentColumns = ["placeId"],
            childColumns = ["placeRefId"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = NCovDataEntity::class,
            parentColumns = ["dataId"],
            childColumns = ["dataRefId"],
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
        )
    ]
)
data class NCovDataItemEntity (
    @PrimaryKey(autoGenerate = true)
    var itemId: Long,
    var latest: Int,

    var coordinateRefId: Long,
    var placeRefId: Long,
    var dataRefId: Long
)


data class NCovDataItemDB (
    var overallLatest: Int,

    var country: String,
    var province: String,
    var latest: Int,

    var lat: Double,
    var _long: Double
)

@Dao
interface NCovDataDBDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun save(coordinate: NCovDataItemCoordinatesEntity): Long
    @Update
    fun update(coordinate: NCovDataItemCoordinatesEntity): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun save(place: NCovDataItemPlaceEntity): Long
    @Update
    fun update(place: NCovDataItemPlaceEntity): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun save(item: NCovDataItemEntity): Long
    @Update
    fun update(item: NCovDataItemEntity): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun save(data: NCovDataEntity): Long
    @Update
    fun update(data: NCovDataEntity): Int

    @Query("SELECT coordinateId FROM NCovDataItemCoordinatesEntity WHERE lat = :lat AND _long = :long")
    fun findCoordinateId(lat: Double, long: Double): Long?

    @Query("SELECT placeId FROM NCovDataItemPlaceEntity WHERE country = :country AND province = :province")
    fun findPlaceId(country: String, province: String): Long?

    @Query("SELECT itemId FROM NCovDataItemEntity WHERE coordinateRefId = :coordinateRefId AND placeRefId = :placeRefId AND dataRefId = :dataRefId")
    fun findItemId(coordinateRefId: Long, placeRefId: Long, dataRefId: Long): Long?

    @Query("SELECT dataId FROM NCovDataEntity WHERE dataId = :dataId")
    fun findDataId(dataId: Long): Long?

    @Transaction
    fun save(ncovData: NCovData, dataId: Int) {
        val data = NCovDataEntity(dataId.toLong(), ncovData.latest)
        var savedDataId = save(data)
        if (savedDataId == -1L) {
            savedDataId = findDataId(data.dataId) ?: return
            update(data)
        }
        ncovData.locations.forEach {
            val coordinate = NCovDataItemCoordinatesEntity(
                0,
                it.coordinates.lat,
                it.coordinates.long
            )
            var savedCoordinateId = save(coordinate)
            if (savedCoordinateId == -1L) {
                savedCoordinateId = findCoordinateId(coordinate.lat, coordinate._long) ?: return
                coordinate.coordinateId = savedCoordinateId
//                update(coordinate)
            }

            val place = NCovDataItemPlaceEntity(
                0,
                it.country.trim(),
                it.province.trim()
            )
            var savedPlaceId = save(place)
            if (savedPlaceId == -1L) {
                savedPlaceId = findPlaceId(place.country, place.province) ?: return
                place.placeId = savedPlaceId
//                update(place)
            }

            val item = NCovDataItemEntity(
                0,
                it.latest,
                savedCoordinateId,
                savedPlaceId,
                savedDataId
            )
            if (save(item) == -1L) {
                val id = findItemId(item.coordinateRefId, item.placeRefId, item.dataRefId) ?: return
                item.itemId = id
                update(item)
            }
        }
    }

    @Transaction
    @Query("SELECT NCovDataEntity.overallLatest, NCovDataItemPlaceEntity.country, NCovDataItemPlaceEntity.province, NCovDataItemEntity.latest, NCovDataItemCoordinatesEntity.lat, NCovDataItemCoordinatesEntity._long FROM NCovDataEntity INNER JOIN NCovDataItemEntity ON NCovDataItemEntity.dataRefId = NCovDataEntity.dataId INNER JOIN NCovDataItemPlaceEntity ON NCovDataItemPlaceEntity.placeId = NCovDataItemEntity.placeRefId INNER JOIN NCovDataItemCoordinatesEntity ON NCovDataItemCoordinatesEntity.coordinateId = NCovDataItemEntity.coordinateRefId WHERE dataId = :data AND NCovDataItemEntity.latest > 0 order by NCovDataItemEntity.latest desc")
    fun load(data: Int): LiveData<List<NCovDataItemDB>>
}

@Database(
    entities = [
        NCovDataEntity::class,
        NCovDataItemEntity::class,
        NCovDataItemPlaceEntity::class,
        NCovDataItemCoordinatesEntity::class
    ],
    version = 1)
abstract class NCovDatabase : RoomDatabase() {
    abstract fun ncovDao(): NCovDataDBDao
}