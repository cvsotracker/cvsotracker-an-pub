/*
 * Created by Marek Stoma on 3/5/20 2:30 AM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 3/5/20 2:28 AM
 */

package org.b25b_48c5_9914_c4878609efe9.cvsotracker.ui.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_search_content.view.*
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.R
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.model.NCovDataItem
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.model.NCovDataValidator
import java.text.NumberFormat


class SearchContentRecyclerViewAdapter(
    private val items : ArrayList<NCovDataItem>,
    private val context: Context,
    private val defaultContentColorRes: Int,
    var contentColorRes: Int?
) : RecyclerView.Adapter<SearchContentViewHolder>() {

    interface OnSelectListener {

        fun onSelectItem(item: NCovDataItem)
    }

    var selectListener: OnSelectListener? = null

    private val numberFormatter = NumberFormat.getNumberInstance()

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchContentViewHolder {
        return SearchContentViewHolder(LayoutInflater.from(context).inflate(R.layout.item_search_content, parent, false))
    }

    override fun onBindViewHolder(holder: SearchContentViewHolder, position: Int) {
        val item = items[position]
        holder.title.text = if (item.province.isNotEmpty()) item.province else "--"
        holder.subtitle.text = if (item.country.isNotEmpty()) item.country else "--"
        holder.content.text = numberFormatter.format(item.latest)
        if (contentColorRes != null && NCovDataValidator.isValid(item.coordinates)) {
            holder.content.setTextColor(contentColorRes!!)
        } else {
            holder.content.setTextColor(defaultContentColorRes)
        }
        holder.itemView.setOnClickListener {
            selectListener?.onSelectItem(item)
        }
    }
}

class SearchContentViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val title: TextView = view.title
    val subtitle: TextView = view.subtitle
    val content: TextView = view.content
}
