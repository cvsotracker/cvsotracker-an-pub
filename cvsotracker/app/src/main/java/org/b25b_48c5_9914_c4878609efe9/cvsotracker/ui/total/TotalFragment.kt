/*
 * Created by Marek Stoma on 3/5/20 2:30 AM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 3/5/20 2:28 AM
 */

package org.b25b_48c5_9914_c4878609efe9.cvsotracker.ui.total

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import org.b25b_48c5_9914_c4878609efe9.cvsotracker.R

class TotalFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_total, container, false)
    }
}
