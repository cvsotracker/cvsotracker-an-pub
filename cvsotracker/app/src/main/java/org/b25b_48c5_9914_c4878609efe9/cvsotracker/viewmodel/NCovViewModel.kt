/*
 * Created by Marek Stoma on 3/5/20 2:30 AM
 * Copyright (c) 2020 . All rights reserved.
 * Last modified 3/5/20 2:28 AM
 */

package org.b25b_48c5_9914_c4878609efe9.cvsotracker.viewmodel

import android.app.Application
import androidx.lifecycle.*
import androidx.room.Room
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.R
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.model.*
import org.b25b_48c5_9914_c4878609efe9.cvsotracker.service.NCovDataServiceExternal
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NCovViewModel(application: Application) : AndroidViewModel(application) {

    enum class NCovOption {
        Confirmed {
            override fun titleTotalRes() = R.string.totalConfirmed
            override fun titleRes() = R.string.confirmed
            override fun colorRes() = R.color.colorConfirmed
        },
        Deaths {
            override fun titleTotalRes() = R.string.totalDeaths
            override fun titleRes() = R.string.deaths
            override fun colorRes() = R.color.colorDeaths
        },
        Recovered {
            override fun titleTotalRes() = R.string.totalRecovered
            override fun titleRes() = R.string.recovered
            override fun colorRes() = R.color.colorRecovered
        },
        Active {
            override fun titleTotalRes() = R.string.totalActive
            override fun titleRes() = R.string.active
            override fun colorRes() = R.color.colorActive
        };

        abstract fun titleTotalRes(): Int
        abstract fun titleRes(): Int
        abstract fun colorRes(): Int
    }

    val stateOption = MutableLiveData(NCovOption.Confirmed)
    val stateSearchContentFilter = MutableLiveData<String>()
    val stateSelectedItem = MutableLiveData<NCovDataItem>()

    private val totalConfirmed: MutableLiveData<NCovData> by lazy {
        MediatorLiveData<NCovData>().also {
            it.addSource(ncovDB.ncovDao().load(NCovOption.Confirmed.ordinal)) {local ->
                val data = convert(local)
                if (data != null) {
                    totalConfirmed.value = data
                }
            }
            loadConfirmed()
        }
    }

    fun outputTotalConfirmed(): LiveData<NCovData> {
        return totalConfirmed
    }

    private fun loadConfirmed() {
        service.getConfirmed().enqueue(object : Callback<NCovData> {
            override fun onFailure(call: Call<NCovData>?, t: Throwable?) {
                if (totalConfirmed.value == null) {
                    totalConfirmed.postValue(null)
                }
            }

            override fun onResponse(call: Call<NCovData>?, response: Response<NCovData>?) {
                val data = response?.body()
                if (data != null) {
                    ncovDB.ncovDao().save(data, NCovOption.Confirmed.ordinal)
                } else if (totalConfirmed.value == null) {
                    totalConfirmed.postValue(null)
                }
            }
        }
        )
    }

    private val totalDeaths: MutableLiveData<NCovData> by lazy {
        MediatorLiveData<NCovData>().also {
            it.addSource(ncovDB.ncovDao().load(NCovOption.Deaths.ordinal)) {local ->
                val data = convert(local)
                if (data != null) {
                    totalDeaths.value = data
                }
            }
            loadDeaths()
        }
    }

    fun outputTotalDeaths(): LiveData<NCovData> {
        return totalDeaths
    }

    private fun loadDeaths() {
        service.getDeaths().enqueue(object : Callback<NCovData> {
            override fun onFailure(call: Call<NCovData>?, t: Throwable?) {
                if (totalDeaths.value == null) {
                    totalDeaths.postValue(null)
                }
            }

            override fun onResponse(call: Call<NCovData>?, response: Response<NCovData>?) {
                val data = response?.body()
                if (data != null) {
                    ncovDB.ncovDao().save(data, NCovOption.Deaths.ordinal)
                } else if (totalDeaths.value == null) {
                    totalDeaths.postValue(null)
                }
            }
        }
        )
    }

    private val totalRecovered: MutableLiveData<NCovData> by lazy {
        MediatorLiveData<NCovData>().also {
            it.addSource(ncovDB.ncovDao().load(NCovOption.Recovered.ordinal)) {local ->
                val data = convert(local)
                if (data != null) {
                    totalRecovered.value = data
                }
            }
            loadRecovered()
        }
    }

    fun outputTotalRecovered(): LiveData<NCovData> {
        return totalRecovered
    }

    private fun loadRecovered() {
        service.getRecovered().enqueue(object : Callback<NCovData> {
            override fun onFailure(call: Call<NCovData>?, t: Throwable?) {
                if (totalRecovered.value == null) {
                    totalRecovered.postValue(null)
                }
            }

            override fun onResponse(call: Call<NCovData>?, response: Response<NCovData>?) {
                val data = response?.body()
                if (data != null) {
                    ncovDB.ncovDao().save(data, NCovOption.Recovered.ordinal)
                } else if (totalRecovered.value == null) {
                    totalRecovered.postValue(null)
                }
            }
        }
        )
    }

    private val activeCases: MutableLiveData<NCovData> by lazy {
        MediatorLiveData<NCovData>().also {
            it.addSource(totalConfirmed) {
                activeCases.value = calculateActiveCases()
            }
            it.addSource(totalDeaths) {
                activeCases.value = calculateActiveCases()
            }
            it.addSource(totalRecovered) {
                activeCases.value = calculateActiveCases()
            }
        }
    }

    fun outputActiveCases(): LiveData<NCovData> {
        return activeCases
    }

    private fun calculateActiveCases(): NCovData? {
        val confirmed = outputTotalConfirmed().value
        val deaths = outputTotalDeaths().value
        val recovered = outputTotalRecovered().value

        if (confirmed != null) {
            val mapDeaths = deaths?.locations?.fold(mutableMapOf<String, Int>()) { result, item ->
                result.also {
                    it[item.country + "-" + item.province] = item.latest
                }
            }
            val mapRecovered = recovered?.locations?.fold(mutableMapOf<String, Int>()) { result, item ->
                result.also {
                    it[item.country + "-" + item.province] = item.latest
                }
            }

            var latest = 0
            val result = confirmed.locations.fold(mutableListOf<NCovDataItem>()) { result, item ->
                result.also {
                    val key = item.country + "-" + item.province
                    val deathValue = mapDeaths?.get(key) ?: 0
                    val recoveredValue = mapRecovered?.get(key) ?: 0
                    val latestValue = item.latest - (deathValue + recoveredValue)
                    latest += latestValue
                    it.add(NCovDataItem(item.country, item.province, latestValue, item.coordinates))
                }
            }
            return sort(NCovData(latest, result))
        } else {
            return null
        }
    }

//    private val service: NCovDataService by lazy {
//        NCovDataService.Factory.create()
//    }
    private val service: NCovDataServiceExternal by lazy {
        NCovDataServiceExternal.Factory.create()
    }
    private val ncovDB: NCovDatabase by lazy {
        Room.databaseBuilder(
            application.applicationContext,
            NCovDatabase::class.java,
            "ncov"
        ).build()
    }

    private fun sort(data: NCovData): NCovData {
        val locations = data.locations.filter {
            it.latest > 0
        }.sortedByDescending {
            it.latest
        }.map {
            NCovDataItem(it.country.trim(), it.province.trim(), it.latest, it.coordinates)
        }
        return NCovData(data.latest, locations)
    }

    private fun convert(local: List<NCovDataItemDB>?): NCovData? {
        if (local != null && local.isNotEmpty()) {
            val locations = local.map { item ->
                NCovDataItem(
                    item.country,
                    item.province,
                    item.latest,
                    NCovDataItemCoordinates(
                        item.lat,
                        item._long
                    )
                )
            }
            return NCovData(
                local.first().overallLatest,
                locations
            )
        } else {
            return null
        }
    }
}
